import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { LogviewComponent } from '~/core/logview.component';
import { IssueReportPageComponent } from '~/pages/issue-report/issue-report.page.component';
import { ProfilePageComponent } from '~/pages/profile/profile.page.component';
import { ProjectsPageComponent } from '~/pages/projects/projects.page.component';
import { SignInPageComponent } from '~/pages/sign-in/sign-in.page.component';
import { TabsPageComponent } from '~/pages/tabs/tabs.page.component';
import { WebviewPageComponent } from '~/pages/webview/webview.page.component';

export const COMPONENTS = [ProfilePageComponent, SignInPageComponent, TabsPageComponent,
  ProjectsPageComponent, IssueReportPageComponent, LogviewComponent, WebviewPageComponent];

const routes: Routes = [
  {path: 'sign-in', component: SignInPageComponent},

  {path: 'tabs', component: TabsPageComponent},
  {path: 'profile', component: ProfilePageComponent, outlet: 'profileTab'},
  {path: 'projects', component: ProjectsPageComponent, outlet: 'projectsTab'},
  {path: 'webview', component: WebviewPageComponent, outlet: 'webviewTab'},
  {path: 'issues-report/:projectKey', component: IssueReportPageComponent},
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {
}
