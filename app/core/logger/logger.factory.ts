import { Logger } from '~/core/logger/logger';

export class LoggerFactory {
  public static readonly root: Logger = new Logger('root');

  public static getLogger(facility: string): Logger {
    return new Logger(facility);
  }
}
