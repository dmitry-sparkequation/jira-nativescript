import { LoggerData } from '~/core/logger/logger.data';

export class Logger {
  constructor(
    private facility: string,
  ) {
  }

  public log(level: string, ...data: Array<any>) {
    if (data.length > 0) {
      data[0] = this.facility + ': ' + data[0];
      LoggerData.log(level, data);
    }
  }

  public info(...data: Array<any>) {
    this.log('INFO', data);
  }

  public warn(...data: Array<any>) {
    this.log('WARN', data);
  }

  public error(...data: Array<any>) {
    this.log('ERROR', data);
  }
}
