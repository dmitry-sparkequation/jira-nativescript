import * as moment from 'moment';

export class LoggerData {
  public static getLogRows(): Array<string> {
    return this.logRows;
  }

  public static log(level: string, data: Array<any>) {
    const timeStr: string = moment.utc().format();
    const dataStr: string = data.join(', ');
    const str = `${timeStr} [${level}] - ` + dataStr;
    this.logRows.push(str);

    switch (level) {
      case 'DEBUG':
        console.debug(str);
        break;
      case 'INFO':
        console.info(str);
        break;
      case 'WARN':
        console.warn(str);
        break;
      case 'ERROR':
        console.error(str);
        break;
      default:
        console.log(str);
        break;
    }
  }

  private static logRows: Array<string> = [];
}
