import { Injectable } from '@angular/core';
import { LoadingIndicator, OptionsCommon } from 'nativescript-loading-indicator';

@Injectable()
export class LoaderService {
  private _shown: boolean;

  private loader: LoadingIndicator = new LoadingIndicator();

  public show(options?: OptionsCommon) {
    if (!this.shown) {
      this.loader.show(options);
      this._shown = true;
    }
  }

  public hide() {
    if (this.shown) {
      this._shown = false;
      this.loader.hide();
    }
  }

  get shown(): boolean {
    return this._shown;
  }
}
