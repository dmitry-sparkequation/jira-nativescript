import { Component } from '@angular/core';
import { LoggerData } from '~/core/logger/logger.data';

@Component({
  selector: 'logview',
  moduleId: module.id,
  template: `<Label text="{{ getText() }}" textWrap="true" borderWidth="1" fontSize="10"></Label>`,
})
export class LogviewComponent {
  public getText(): string {
    return LoggerData.getLogRows().join('\n');
  }
}
