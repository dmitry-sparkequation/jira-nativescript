import { AndroidActivityBundleEventData } from 'application';

export class IntentHandler {

  public static action: any;
  public static type: any;
  public static sharedText: string;

  public static androidAppOnActivityCreated(args: AndroidActivityBundleEventData) {
    const intent = args.activity.getIntent();
    this.action = intent.getAction();
    this.type = intent.getType();
    this.sharedText = intent.getStringExtra('android.intent.extra.TEXT');

    /*
    if (this.action) {
      LoggerFactory.root.info(`!!! Received intent action from share: action: ${this.action}\ntype: ${this.type}`);
    }
    */
  }

  public static resetData() {
    this.action = null;
    this.type = null;
    this.sharedText = null;
  }
}
