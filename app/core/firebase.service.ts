import {Injectable} from '@angular/core';
import * as firebase from 'nativescript-plugin-firebase';
import {GetRemoteConfigResult, Message} from 'nativescript-plugin-firebase';
import * as dialogs from 'ui/dialogs';
import {LoggerFactory} from '~/core/logger/logger.factory';

@Injectable()
export class FirebaseService {

  private log = LoggerFactory.getLogger('FirebaseService');

  public async init(): Promise<void> {
    await firebase.init({
      onPushTokenReceivedCallback: (token) => this.onPushTokenReceivedCallback(token),
      onMessageReceivedCallback: (data) => this.onMessageReceived(data),
    }).catch((error) => {
      this.log.error('Error initalizing', JSON.stringify(error));
    });

    firebase.getCurrentPushToken().catch((error) => {
      this.log.error('Error getCurrentPushToken', JSON.stringify(error));
    });

    /*
    setTimeout(() => {
      firebase.admob.showBanner({
        size: firebase.admob.AD_SIZE.BANNER,
        testing: true,
        androidBannerId: 'ca-app-pub-3940256099942544/6300978111',
        iosBannerId: 'ca-app-pub-3940256099942544/6300978111',
        margins: {
          bottom: 50
        },
      }).then(() => {
        this.log.info('Admob banner shown');
      }).catch((error) => {
        this.log.error('Error showing admob banner', error, JSON.stringify(error.stack));
      });
    }, 5000);
    */

    setTimeout(() => {
      // `getRemoteConfig` works only inside `setTimeout` for Android. For iOS the setTimeout
      // doesn't necessary
      firebase.getRemoteConfig({
        developerMode: false,
        cacheExpirationSeconds: 60 * 30,
        properties: [{key: 'foo', default: ''}],
      }).then((result: GetRemoteConfigResult) => {
        this.log.info('Got remote config: ' + JSON.stringify(result));
      }).catch((error) => {
        this.log.error('Error getRemoteConfig', JSON.stringify(error.stack));
      });
    }, 0);

    this.log.info('Started');
  }

  private onMessageReceived(data: Message) {
    this.log.info('onMessageReceived', JSON.stringify(data));

    dialogs.alert({
      title: 'onMessageReceived',
      message: JSON.stringify(data)
    });
  }

  private onPushTokenReceivedCallback(token: string) {
    this.log.info('onPushTokenReceived: ' + token);
  }
}
