import { NgModule } from '@angular/core';
import { LoaderService } from '~/core/loader.service';

@NgModule({
  providers: [
    LoaderService,
  ]
})
export class CoreModule {
}
