import { Injectable } from '@angular/core';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { JiraApiResource } from '~/resources/jira';
import { Project } from '~/resources/jira/project';

@Injectable()
export class ProjectsService {

  private initialized: boolean = false;

  private _projects: ObservableArray<Project> = new ObservableArray();

  private _curProject?: Project;

  constructor(
    private jiraApiService: JiraApiResource
  ) {
  }

  public async lazyFetchProjects() {
    if (!this.initialized) {
      await this.fetchProjects();
    }
  }

  public async fetchProjects() {
    this._projects = new ObservableArray(await this.jiraApiService.getAllProjects());
    this.initialized = true;
  }

  public isInitialized(): boolean {
    return this.initialized;
  }

  get projects(): ObservableArray<Project> {
    return this._projects;
  }

  get curProject(): Project {
    return this._curProject;
  }

  set curProject(value: Project) {
    this._curProject = value;
  }
}
