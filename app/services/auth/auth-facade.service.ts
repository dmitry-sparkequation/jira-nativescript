import { Injectable } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import { ProfileService } from '~/services/profile/profile.service';
// import { SignInPage } from '../../pages/sign-in/sign-in.component';
// import { ProfileService } from '../profile/profile.service';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthFacade {
  constructor(
    private profileService: ProfileService,
    private authenticationService: AuthenticationService,
    private routerExtensions: RouterExtensions,
  ) {
  }

  public async afterSignIn() {
    await Promise.all([
      this.profileService.fetchProfile()
    ]);
  }

  public async doLogout() {
    await this.authenticationService.removeSession();

    await this.routerExtensions.navigate(['/sign-in']);
  }
}
