import { Injectable } from '@angular/core';
import * as appSettings from 'application-settings';

@Injectable()
export class AuthenticationService {
  private static readonly USERNAME_KEY = 'username';
  private static readonly PASSWORD_KEY = 'password';

  private readyPromise: Promise<any>;

  private _username: string = null;
  private _password: string = null;

  constructor() {
    this._username = appSettings.getString(AuthenticationService.USERNAME_KEY);
    this._password = appSettings.getString(AuthenticationService.PASSWORD_KEY);
    this.readyPromise = Promise.resolve();
  }

  ready(): Promise<any> {
    return this.readyPromise;
  }

  isAuthenticated(): boolean {
    return !!this.username && !!this.password;
  }

  get username(): string {
    return this._username;
  }

  get password(): string {
    return this._password;
  }

  setSession(username: string, password: string) {
    this._username = username;
    this._password = password;
    appSettings.setString(AuthenticationService.USERNAME_KEY, username);
    appSettings.setString(AuthenticationService.PASSWORD_KEY, password);
  }

  removeSession() {
    this._username = null;
    this._password = null;
    appSettings.remove(AuthenticationService.USERNAME_KEY);
    appSettings.remove(AuthenticationService.PASSWORD_KEY);
  }
}
