import { Injectable } from '@angular/core';
import { JiraApiResource } from '../../resources/jira';
import { Project } from '../../resources/jira/project';

@Injectable()
export class IssuesService {

  private _issuesOpen: number;
  private _issuesInProgress: number;
  private _issuesClosed: number;

  constructor(
    private jiraApiResource: JiraApiResource,
  ) {
  }

  public reset() {
    this._issuesOpen = null;
    this._issuesInProgress = null;
    this._issuesClosed = null;
  }

  public async fillReportForProject(project: Project) {
    const doQuery = async (status: string) => {
      return (await this.jiraApiResource.search({maxResults: 0, jql: `project = ${project.key} AND status = "${status}"`})).total;
    };
    this._issuesOpen = await doQuery('Open');
    this._issuesInProgress = await doQuery('In Progress');
    this._issuesClosed = await doQuery('Closed');
  }

  get issuesOpen(): number {
    return this._issuesOpen;
  }

  get issuesInProgress(): number {
    return this._issuesInProgress;
  }

  get issuesClosed(): number {
    return this._issuesClosed;
  }
}
