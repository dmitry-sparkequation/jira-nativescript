import { Injectable } from '@angular/core';
import { JiraApiResource, MyselfResult } from '../../resources/jira';

@Injectable()
export class ProfileService {
  private _name: string;
  private _email: string;
  private _displayName: string;

  constructor(
    private jiraApiService: JiraApiResource,
  ) {
  }

  public async fetchProfile() {
    try {
      const myself: MyselfResult = await this.jiraApiService.myself();
      this._name = myself.name;
      this._email = myself.emailAddress;
      this._displayName = myself.displayName;
    } catch (error) {
      // alert('Unable to fetch profile: ' + JSON.stringify(error));
      console.error('Unable to fetch profile: ' + JSON.stringify(error));
    }
  }

  get name(): string {
    return this._name;
  }

  get email(): string {
    return this._email;
  }

  get displayName(): string {
    return this._displayName;
  }
}
