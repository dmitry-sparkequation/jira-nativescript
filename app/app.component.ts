import { Component } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import * as dialogs from 'ui/dialogs';
import { FirebaseService } from '~/core/firebase.service';
import { IntentHandler } from '~/core/intent-handlers';
import { LoggerFactory } from '~/core/logger/logger.factory';
import { AuthFacade } from '~/services/auth/auth-facade.service';
import { AuthenticationService } from '~/services/auth/authentication.service';

@Component({
  selector: 'ns-app',
  moduleId: module.id,
  template: `
    <page-router-outlet></page-router-outlet>`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private log = LoggerFactory.getLogger('App');

  constructor(
    private authenticationService: AuthenticationService,
    private authFacade: AuthFacade,
    private routerExtensions: RouterExtensions,
    private firebaseService: FirebaseService,
  ) {
    const initFn = async () => {
      await Promise.all([
        firebaseService.init()
      ]);

      await this.authenticationService.ready();
      if (this.authenticationService.isAuthenticated()) {
        await this.authFacade.afterSignIn();
      }
      this.checkIntentHandler();
      this.defineRootPage();
    };
    initFn();
  }

  protected checkIntentHandler() {
    if (IntentHandler.action && IntentHandler.sharedText) {
      setTimeout(() => {
        dialogs.alert({
          title: 'Received intent action from share',
          message: `Action: ${IntentHandler.action}\ntype: ${IntentHandler.type}\n\ntext: ${IntentHandler.sharedText}`,
          okButtonText: 'OK'
        });
        IntentHandler.resetData();
      }, 100);
    }
  }

  protected defineRootPage(): void {
    if (this.authenticationService.isAuthenticated()) {
      this.log.info('authenticated');
      this.routerExtensions.navigate(['tabs']);
    } else {
      this.log.info('not signed in');
      this.routerExtensions.navigate(['sign-in']);
    }
  }
}
