import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptFormsModule, registerElement } from 'nativescript-angular';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { NativeScriptUIChartModule } from 'nativescript-ui-chart/angular';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';
import { FirebaseService } from '~/core/firebase.service';
import { JiraApiResource, JiraAuthResource } from '~/resources/jira';
import { AuthFacade } from '~/services/auth/auth-facade.service';
import { AuthenticationService } from '~/services/auth/authentication.service';
import { IssuesService } from '~/services/issues/issues.service';
import { ProfileService } from '~/services/profile/profile.service';
import { ProjectsService } from '~/services/projects/projects.service';

import { AppRoutingModule, COMPONENTS } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

@NgModule({
  bootstrap: [
    AppComponent
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    CoreModule,
    NativeScriptFormsModule,
    NativeScriptUIChartModule,
    NativeScriptUIListViewModule,
  ],
  declarations: [
    AppComponent,
    ...COMPONENTS,
  ],
  providers: [
    ProfileService,
    AuthFacade,
    AuthenticationService,
    JiraApiResource,
    JiraAuthResource,
    ProjectsService,
    IssuesService,
    FirebaseService,
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule {
}

registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager").PreviousNextView);
