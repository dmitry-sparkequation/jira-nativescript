import { Injectable } from '@angular/core';
import { JiraBaseResource } from '~/resources/jira/jira-base.resource';
import { LoginResult } from './login-result';

@Injectable()
export class JiraAuthResource extends JiraBaseResource {

  public login(data: { username: string, password: string }): Promise<LoginResult> {
    return this.doRequest({
      url: this.pathPrefix() + '/session',
      method: 'POST',
    }, data);
  }

  // @ResourceAction({
  //   method: ResourceRequestMethod.Delete,
  //   path: '/session'
  // })
  public logout(): Promise<void> {
    return this.doRequest({
      url: this.pathPrefix() + '/session',
      method: 'DELETE',
    });
  }

  //
  // @ResourceAction({
  //   method: ResourceRequestMethod.Get,
  //   path: '/session'
  // })
  // currentUser: IResourceMethod<void, CurrentUserResult>;

  private pathPrefix(): string {
    return this.apiEndpoint + '/rest/auth/1';
  }
}
