import { Injectable } from '@angular/core';
import { JiraBaseResource } from './jira-base.resource';
import { MyselfResult } from './myself-result';
import { Project } from './project';
import { SearchResult } from './search-result';

@Injectable()
export class JiraApiResource extends JiraBaseResource {

  public myself(): Promise<MyselfResult> {
    return this.doRequest({
      url: this.pathPrefix() + '/myself',
      method: 'GET'
    });
  }

  public getAllProjects(data?: { expand?: string, recent?: number, includeArchived?: boolean }): Promise<Array<Project>> {
    return this.doRequest({
      url: this.applyQueryData(this.pathPrefix() + '/project', data),
      method: 'GET'
    });
  }

  public search(data?: { maxResults?: number, jql: string }): Promise<SearchResult> {
    return this.doRequest({
      url: this.applyQueryData(this.pathPrefix() + '/search', data),
      method: 'GET'
    });
  }

  private pathPrefix(): string {
    return this.apiEndpoint + '/rest/api/2';
  }
}
