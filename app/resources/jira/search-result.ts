export class SearchResult {
  // Example:
  // "expand": "names,schema",
  // "startAt": 0,
  // "maxResults": 50,
  // "total": 1,
  // "issues": [
  //   {
  //     "expand": "",
  //     "id": "10001",
  //     "self": "http://www.example.com/jira/rest/api/2/issue/10001",
  //     "key": "HSP-1"
  //   }
  // ]

  expand: string;
  startAt: number;
  maxResults: number;
  total: number;
  issues: [{
    expand: string;
    id: string;
    self: string;
    key: string;
  }];
}
