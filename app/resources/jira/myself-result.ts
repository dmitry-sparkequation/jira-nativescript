export class MyselfResult {
  /* Example:
  "self": "http://www.example.com/jira/rest/api/2/user?username=fred",
  "name": "fred",
  "emailAddress": "fred@example.com",
  "avatarUrls": {
    "48x48": "http://www.example.com/jira/secure/useravatar?size=large&ownerId=fred",
    "24x24": "http://www.example.com/jira/secure/useravatar?size=small&ownerId=fred",
    "16x16": "http://www.example.com/jira/secure/useravatar?size=xsmall&ownerId=fred",
    "32x32": "http://www.example.com/jira/secure/useravatar?size=medium&ownerId=fred"
  },
  "displayName": "Fred F. User",
  "active": true,
  "timeZone": "Australia/Sydney",
  "groups": {
    "size": 3,
    "items": [
      {
        "name": "jira-user",
        "self": "http://www.example.com/jira/rest/api/2/group?groupname=jira-user"
      },
      {
        "name": "jira-admin",
        "self": "http://www.example.com/jira/rest/api/2/group?groupname=jira-admin"
      },
      {
        "name": "important",
        "self": "http://www.example.com/jira/rest/api/2/group?groupname=important"
      }
      ]
  },
  "applicationRoles": {
    "size": 1,
    "items": []
  },
  "expand": "groups,applicationRoles"
  */

  self: string;
  name: string;
  emailAddress: string;
  avatarUrls: object;
  displayName: string;
  active: boolean;
  timeZone: string;
  groups: {
    size: number;
    items: [{
      name: string,
      self: string
    }];
  };
  applicationRoles: {
    size: number,
    items: [any];
  };
  expand: string;
}
