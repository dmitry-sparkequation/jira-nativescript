export class LoginResult {
  session: {
    name: string,
    value: string
  };
}
