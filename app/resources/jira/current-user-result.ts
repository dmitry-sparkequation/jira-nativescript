export class CurrentUserResult {
  // example: http://www.example.com/jira/rest/api/2.0/user/fred
  self: string;

  // example: "fred"
  name: string;

  // example: {
  //   "failedLoginCount": 10,
  //   "loginCount": 127,
  //   "lastFailedLoginTime": "2018-08-08T11:58:02.342+0000",
  //   "previousLoginTime": "2018-08-08T11:58:02.342+0000"
  // }
  loginInfo: any;
}
