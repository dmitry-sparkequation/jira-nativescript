import { Injectable } from '@angular/core';
import * as  base64 from 'base-64';
import { HttpRequestOptions } from 'tns-core-modules/http';
import { AuthenticationService } from '~/services/auth/authentication.service';
// import * as dialogs from "ui/dialogs";

import * as httpModule from 'http';

@Injectable()
export class JiraBaseResource {
  constructor(
    protected authenticationService: AuthenticationService,
    // protected alertCtrl: AlertController,
  ) {
    // super(handler);
  }

  // public $getUrl(actionOptions?: IResourceAction): string | Promise<string> {
  //   return 'https://jira.sparkequation.com'; // TODO: move to the config
  //   // return 'http://10w10pc03.corp.sparkequation.com:8001';
  // }
  //
  // public $getHeaders(actionOptions?: IResourceAction): any | Promise<any> {
  //   if (this.authenticationService.isAuthenticated()) {
  //     const authString = btoa(this.authenticationService.username + ':' + this.authenticationService.password);
  //     return {'Authorization': 'Basic ' + authString};
  //   } else {
  //     return {};
  //   }
  // }
  //
  // public $restAction(options: IResourceActionInner): any {
  //   const result: Promise<any> = super.$restAction(options);
  //   result.catch((error: { status: number }) => {
  //     if (error.status === 0) {
  //       // No connection
  //       this.alertCtrl.create({
  //         subTitle: 'Unable to make request - no connection to the server',
  //         buttons: ['Ok']
  //       }).present();
  //
  //     } else if (error.status === 401) {
  //       if (options.actionOptions.ignore401) {
  //         return null;
  //       } else {
  //         this.alertCtrl.create({
  //           subTitle: 'Got unauthorized request',
  //           buttons: ['Ok']
  //         }).present();
  //         // return authFacade.onUnauthorizedRequest();
  //       }
  //
  //     } else {
  //       console.error('Error requesting ' + JSON.stringify(error));
  //       return null;
  //     }
  //   });
  //   return result;
  // }

  protected get apiEndpoint(): string {
    return 'https://jira.sparkequation.com';
  }

  protected doRequest<T>(options: HttpRequestOptions, data?: any): Promise<T> {
    if (!options.headers) {
      options.headers = {};
    }
    if (!options.headers['Content-Type']) {
      options.headers['Content-Type'] = 'application/json';
    }
    if (!options.headers.Authorization && this.authenticationService.isAuthenticated()) {
      const authString = base64.encode(this.authenticationService.username + ':' + this.authenticationService.password);
      options.headers.Authorization = 'Basic ' + authString;
    }
    if (!options.content && data) {
      options.content = JSON.stringify(data);
    }

    return httpModule.request(options)
      .then((response) => {
        return response.content.toJSON();
      });
  }

  protected encodeQueryData(data: object): string {
    const ret = [];
    for (const d in data) {
      ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    }
    return ret.join('&');
  }

  protected applyQueryData(url: string, data?: object): string {
    if (data) {
      return url + '?' + this.encodeQueryData(data);
    } else {
      return url;
    }
  }
}
