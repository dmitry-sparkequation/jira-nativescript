// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScript } from 'nativescript-angular/platform-static';
import { android, AndroidActivityBundleEventData, AndroidApplication } from 'tns-core-modules/application';
import { IntentHandler } from '~/core/intent-handlers';

import { AppModuleNgFactory } from './app.module.ngfactory';

if (android) {
  android.on(AndroidApplication.activityCreatedEvent, (args: AndroidActivityBundleEventData) => IntentHandler.androidAppOnActivityCreated(args));
}

platformNativeScript().bootstrapModuleFactory(AppModuleNgFactory);
