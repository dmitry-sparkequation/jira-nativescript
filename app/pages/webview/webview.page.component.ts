import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Label } from 'tns-core-modules/ui/label';
import { TextField } from 'tns-core-modules/ui/text-field';
import { LoadEventData, WebView } from 'tns-core-modules/ui/web-view';

@Component({
  selector: 'page-webview',
  moduleId: module.id,
  templateUrl: 'webview.page.component.html',
})
export class WebviewPageComponent implements AfterViewInit {
  public webViewSrc: string = 'https://docs.nativescript.org/';

  @ViewChild('myWebView') webViewRef: ElementRef;
  @ViewChild('urlField') urlFieldRef: ElementRef;
  @ViewChild('labelResult') labelResultRef: ElementRef;

  public ngAfterViewInit() {
    const webview: WebView = this.webViewRef.nativeElement;
    const label: Label = this.labelResultRef.nativeElement;
    label.text = 'WebView is still loading...';

    webview.on(WebView.loadFinishedEvent, (args: LoadEventData) => {
      let message;
      if (!args.error) {
        message = 'WebView finished loading of ' + args.url;
      } else {
        message = 'Error loading ' + args.url + ': ' + args.error;
      }

      label.text = message;
      console.log('WebView message - ' + message);
    });
  }

  public goBack() {
    const webview: WebView = this.webViewRef.nativeElement;
    if (webview.canGoBack) {
      webview.goBack();
    }
  }

  public submit(args: string) {
    const textField: TextField = this.urlFieldRef.nativeElement;

    if (args.substring(0, 4) === 'http') {
      this.webViewSrc = args;
      textField.dismissSoftInput();
    } else {
      alert('Please, add `http://` or `https://` in front of the URL string');
    }
  }
}
