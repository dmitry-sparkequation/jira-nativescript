import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import { LoaderService } from '~/core/loader.service';
import { JiraAuthResource, LoginResult } from '~/resources/jira';
import { AuthFacade } from '~/services/auth/auth-facade.service';
import { AuthenticationService } from '~/services/auth/authentication.service';
import { Page } from 'tns-core-modules/ui/page';

@Component({
  selector: 'page-sign-in',
  moduleId: module.id,
  templateUrl: 'sign-in.page.component.html',
  styleUrls: ['./sign-in.css'],
})
export class SignInPageComponent implements OnInit {
  public username: string;
  public password: string;

  public error: string;

  constructor(
    page: Page,
    private jiraAuthResource: JiraAuthResource,
    private authenticationService: AuthenticationService,
    private authFacade: AuthFacade,
    private routerExtensions: RouterExtensions,
    private loaderService: LoaderService,
  ) {
    page.actionBarHidden = true;
  }

  ngOnInit(): void {
  }

  public onSubmit() {
    if (!this.username) {
      this.error = 'Username is required';
    } else if (!this.password) {
      this.error = 'Password is required';
    } else {
      this.error = '';
      this.signIn();
    }
  }

  private async signIn() {
    this.loaderService.show({
      message: 'Signing in...'
    });
    try {
      let authResp: LoginResult;
      try {
        authResp = await this.jiraAuthResource.login({username: this.username, password: this.password});
      } catch (e) {
        this.error = 'The Username or Password is incorrect.';
        return;
      }
      if (!authResp || !authResp.session || !authResp.session.name || !authResp.session.value) {
        this.error = 'The Username or Password is incorrect.';
        return;
      }
      this.authenticationService.setSession(this.username, this.password);

      await this.authFacade.afterSignIn();

      this.routerExtensions.navigate(['tabs'], {clearHistory: true});

    } finally {
      this.loaderService.hide();
    }
  }
}
