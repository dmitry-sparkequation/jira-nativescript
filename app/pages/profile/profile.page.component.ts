import { Component } from '@angular/core';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { ProfileService } from '~/services/profile/profile.service';

@Component({
  selector: 'page-profile',
  moduleId: module.id,
  templateUrl: 'profile.page.component.html'
})
export class ProfilePageComponent {

  public listData: ObservableArray<string> = new ObservableArray<string>([]);

  constructor(
    public profileService: ProfileService,
  ) {
    this.listData.push([
      `name: ${profileService.name}`,
      `email: ${profileService.email}`,
      `displayName: ${profileService.displayName}`,
    ]);
  }
}
