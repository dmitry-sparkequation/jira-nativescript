import { AfterViewInit, Component } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import { ProjectsService } from '~/services/projects/projects.service';

@Component({
  selector: 'page-projects',
  moduleId: module.id,
  templateUrl: 'projects.page.component.html'
})
export class ProjectsPageComponent implements AfterViewInit {

  constructor(
    public projectsService: ProjectsService,
    public routerExtensions: RouterExtensions,
  ) {
  }

  public async ngAfterViewInit() {
    await this.projectsService.lazyFetchProjects();
  }

  public onProjectSelected(event) {
    const index = event.index;
    const project = this.projectsService.projects.getItem(index);
    this.projectsService.curProject = project;
    this.routerExtensions.navigate(['/issues-report', project.key]);
  }
}
