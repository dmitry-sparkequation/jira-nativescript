import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { Project } from '~/resources/jira/project';
import { IssuesService } from '~/services/issues/issues.service';
import { ProjectsService } from '~/services/projects/projects.service';

@Component({
  selector: 'page-issue-report',
  moduleId: module.id,
  templateUrl: 'issue-report.page.component.html',
})
export class IssueReportPageComponent implements OnInit {

  public project: Project;

  public pieSource: ObservableArray<IPieItem> = new ObservableArray([]);

  constructor(
    // public navParams: NavParams,
    public issuesService: IssuesService,
    projectsService: ProjectsService,
    route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
  ) {
    const projectKey = route.snapshot.params.projectKey;
    const foundProjects = projectsService.projects.filter((v: Project) => v.key === projectKey);
    if (!foundProjects.length) {
      throw new Error(`Project with key ${projectKey} not found`);
    }
    this.project = foundProjects[0];
  }

  public async ngOnInit() {
    this.issuesService.reset();
    await this.issuesService.fillReportForProject(this.project);

    this.pieSource.push([
      {title: 'issues open', value: +this.issuesService.issuesOpen},
      {title: 'issues in progress', value: this.issuesService.issuesInProgress},
      {title: 'issues closed', value: this.issuesService.issuesClosed}]);
  }

  public goBack() {
    this.routerExtensions.backToPreviousPage();
  }
}

interface IPieItem {
  title: string;
  value: number;
}
