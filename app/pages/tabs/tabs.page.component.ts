import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import { isAndroid } from 'tns-core-modules/platform';
import { AuthFacade } from '~/services/auth/auth-facade.service';
import { ProfileService } from '~/services/profile/profile.service';
import { TabView } from 'tns-core-modules/ui/tab-view';
import * as application from "application";

@Component({
  selector: 'page-tabs',
  moduleId: module.id,
  templateUrl: './tabs.page.component.html',
})
export class TabsPageComponent implements OnInit {
  public actionBarTitle: string;

  public isAndroid: boolean;
  public isIos: boolean;

  constructor(
    private routerExtensions: RouterExtensions,
    public profileService: ProfileService,
    private authFacade: AuthFacade,
  ) {
  }

  ngOnInit(): void {
    if (application.ios) {
      this.isAndroid = false;
      this.isIos = true;
    } else if (application.android) {
      this.isAndroid = true;
      this.isIos = false;
    }
  }

  onLogout() {
    this.authFacade.doLogout();
  }

  public getIconSource(icon: string): string {
    const iconPrefix = isAndroid ? 'res://' : 'res://tabIcons/';

    return iconPrefix + icon;
  }

  public onIndexChanged(event) {
    const tabView = <TabView>event.object;
    switch (tabView.selectedIndex) {
      case 0:
        this.actionBarTitle = 'User profile';
        break;

      case 1:
        this.actionBarTitle = 'Active projects';
        break;

      case 2:
        this.actionBarTitle = 'WebView';
        break;
    }
  }
}
