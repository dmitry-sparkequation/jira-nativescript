// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { android, AndroidActivityBundleEventData, AndroidApplication } from 'application';
import { platformNativeScriptDynamic } from 'nativescript-angular/platform';
import { IntentHandler } from '~/core/intent-handlers';

import { AppModule } from './app.module';

if (android) {
  android.on(AndroidApplication.activityCreatedEvent, (args: AndroidActivityBundleEventData) => IntentHandler.androidAppOnActivityCreated(args));
}

platformNativeScriptDynamic().bootstrapModule(AppModule);
